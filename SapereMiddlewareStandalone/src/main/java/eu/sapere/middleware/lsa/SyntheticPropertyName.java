package eu.sapere.middleware.lsa;

/**
 * Synthetic Properties
 * 
 */
public enum SyntheticPropertyName {

//	CREATION_TIME("creationTime"), 
//	CREATOR_ID("creatorId"), 
//	LAST_MODIFIED("lastModified"), 
	DECAY("decay"),
	QUERY("query"), //Query LSA
	REWARD("reward"), //Reward LSA
	OUTPUT("output"), 
	SOURCE("source"),
	BOND("bond"),
	DESTINATION("destination"),
	TYPE("type"),
	STATE("state"),
	DIFFUSE("diffuse"),
	PREVIOUS("previous"),
	GRADIENT_HOP("gradient_hop"),
	//QOS("qos"),
	LOCATION("location");

	private SyntheticPropertyName(final String text) {
		this.text = text;
	}

	private final String text;

	@Override
	public String toString() {
		return text;
	}

}
