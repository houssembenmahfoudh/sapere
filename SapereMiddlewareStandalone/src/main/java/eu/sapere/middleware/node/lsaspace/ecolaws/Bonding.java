package eu.sapere.middleware.node.lsaspace.ecolaws;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Space;
import eu.sapere.middleware.node.networking.transmission.NetworkDeliveryManager;
import eu.sapere.middleware.node.notifier.Notifier;
import eu.sapere.middleware.node.notifier.event.AbstractSapereEvent;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.PrintUtil;
import org.apache.jena.vocabulary.RDFS;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Bonding extends AbstractEcoLaw {

    /**
     * Creates a new instance of the bonding eco-law.
     *
     * @param space                  The space in which the eco-law executes.
     * @param opManager              The OperationManager that manages operations in
     *                               the space
     * @param notifier               The Notifier that notifies agents with events
     *                               happening to LSAs
     * @param networkDeliveryManager The interface for Network Delivery of LSAs
     */
    public Bonding(Space space, OperationManager opManager, Notifier notifier,
                   NetworkDeliveryManager networkDeliveryManager) {
        super(space, opManager, notifier, networkDeliveryManager);
    }

    @Override
    public void invoke() {
        execBondsFromLSA();
    }

    private void execBondsFromLSA() {
        for (Lsa outerLsa : getLSAs().values()) {
            if (!outerLsa.isSubdescriptionEmpty()) {
                for (Lsa targetLsa : getLSAs().values()) {
                    if (semantic_match(outerLsa, targetLsa) && outerLsa.shouldBound(targetLsa)
                            && !targetLsa.checkNullPropertiesByQuery(
                            targetLsa.getSyntheticProperty(SyntheticPropertyName.QUERY).toString())
                            && !targetLsa.getProperties().isEmpty() && !outerLsa.getAgentName().contains("*")) {
                        bondLSAToLSA(outerLsa, targetLsa);
                    }
                }
            }
        }
    }

    /**
     * check if input matches with at least one output
     *
     * @param outerLsa
     * @param targetLsa
     * @return
     */
    public boolean syntactic_match(Lsa outerLsa, Lsa targetLsa) {
        for (String subdesc : outerLsa.getSubDescription()) {
            if (targetLsa.hasSyntheticProperty(SyntheticPropertyName.OUTPUT)) {
                for (String s : targetLsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT).toString().split(",")) {
                    if (s.equals(subdesc)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void printStatements(Model m, Resource s, Property p, Resource o) {
        for (StmtIterator i = m.listStatements(s, p, o); i.hasNext(); ) {
            Statement stmt = i.nextStatement();
            System.out.println(" - " + PrintUtil.print(stmt));
        }
    }

    // outerLSA Parking - s3
    // targetLSA GasStation - s2
    // GasStation with Parking --> GasStation --isComposedOf--> Parking
    public boolean semantic_match(Lsa outerLsa, Lsa targetLsa) {
        final String baseURI = "http://cui.unige.ch/isi/onto/MDPI-JSAN#";
        Model schema = ModelFactory.createDefaultModel();
        schema.read(this.getClass().getClassLoader().getResourceAsStream("mdpi-jsan-onto.ttl"), null, "TTL");


        //Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
        //reasoner = reasoner.bindSchema(schema);

        //Model data = ModelFactory.createDefaultModel();


        List<String> outerLsaSubDescriptions = outerLsa.getSubDescription()
                .stream()
                .map(baseURI::concat)
                .collect(Collectors.toList());

        List<String> targetLsaProps = Arrays.stream(targetLsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT).toString()
                .split(","))
                .map(baseURI::concat)
                .collect(Collectors.toList());

        // TODO evaluate inferences and generate inference data model

        for (String out : outerLsaSubDescriptions) {
            Resource r1 = schema.getResource(out);
            for (String target : targetLsaProps) {
                Resource r2 = schema.getResource(target);
                if (schema.contains(r2, RDFS.subClassOf, r1)) {
                    System.out.println("OuterLSA :" + r1.getLocalName());
                    System.out.println("TargetLSA :" + r2.getLocalName());
                    outerLsa.replaceSyntheticProperty(SyntheticPropertyName.OUTPUT, r2.getLocalName());
                    return true;
                } else {
                    Property composedRelationship = ResourceFactory.createProperty(baseURI, "isComposedOf");
                    if (schema.contains(r2, composedRelationship, r1)) {
                        outerLsa.replaceSyntheticProperty(SyntheticPropertyName.OUTPUT, r2.getLocalName());
                        return true;
                    }
                }
                System.out.println("FALSE CASE\n");
                System.out.println("OuterLSA :" + r1.getLocalName() + "\n");
                System.out.println("TargetLSA :" + r2.getLocalName());
            }
        }
        return false;
    }

    private void bondLSAToLSA(Lsa outerLsa, Lsa targetLsa) {
        outerLsa.addSyntheticProperty(SyntheticPropertyName.BOND, targetLsa.getAgentName());
        if (!outerLsa.getSyntheticProperty(SyntheticPropertyName.TYPE).equals(LsaType.Query)) {
            outerLsa.addSyntheticProperty(SyntheticPropertyName.QUERY,
                    targetLsa.getSyntheticProperty(SyntheticPropertyName.QUERY));
        }

        if (!outerLsa.hasBondedBefore(targetLsa.getAgentName(),
                targetLsa.getSyntheticProperty(SyntheticPropertyName.QUERY).toString())) {
            AbstractSapereEvent lsaBondedEvent = new BondEvent(outerLsa, targetLsa);
            lsaBondedEvent.setRequiringAgent(outerLsa.getAgentName());
            publish(lsaBondedEvent);
        }
    }
}
