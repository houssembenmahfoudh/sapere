import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  
  greeting = {};
  constructor( private http: HttpClient) {
    //this.http.get('http://localhost:9090/resource').subscribe(data => this.greeting = data);
  }
  
  authenticated() { return true; }
}
