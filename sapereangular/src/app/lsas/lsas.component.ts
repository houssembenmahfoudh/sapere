import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConstantsService } from '../common/services/constants.service';

@Component({
  selector: 'app-lsas',
  templateUrl: './lsas.component.html',
  styleUrls: ['./lsas.component.scss']
})
export class LsasComponent implements OnInit {
  lsas = [];

  constructor(private httpClient: HttpClient,private _constant: ConstantsService) { 
    this.httpClient.get(this._constant.baseAppUrl+'lsas').
      subscribe((res :any[])=> {
        this.lsas=res;
      })
  }

  ngOnInit() {
  }

}
