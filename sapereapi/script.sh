#!/bin/bash

mvn install:install-file -Dfile=sapereV1.jar -DgroupId=com.sapere -DartifactId=sapere -Dversion=1.0 -Dpackaging=jar

mvn clean package

docker build -t houcembenmahfoudh/springbootapi:1 .
docker push houcembenmahfoudh/springbootapi:1

