package com.sapereapi.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={ "com.sapereapi.api" ,
		"com.sapereapi.entity", "com.sapereapi.model"})
public class SapereAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(SapereAPIApplication.class, args);
	}
}
